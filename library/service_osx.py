#!/usr/bin/python
# -*- coding: utf-8 -*-

import platform
import os
import re
import tempfile
import shlex
import select
import time
import string
import glob
import json

class ServiceOSX(object):

    def __init__(self, module):
        self.module = module
        self.name = module.params['name']
        self.path = module.params['path']
        self.state = module.params['state']
        self.plist_name = module.params['plist_name']
        self.current_state = None
        self.changed = False
        self.get_state()

    def execute_command(self, cmd, daemonize=False):

        # Most things don't need to be daemonized
        if not daemonize:
            return self.module.run_command(cmd)

        # This is complex because daemonization is hard for people.
        # What we do is daemonize a part of this module, the daemon runs the
        # command, picks up the return code and output, and returns it to the
        # main process.
        pipe = os.pipe()
        pid = os.fork()
        if pid == 0:
            os.close(pipe[0])
            # Set stdin/stdout/stderr to /dev/null
            fd = os.open(os.devnull, os.O_RDWR)
            if fd != 0:
                os.dup2(fd, 0)
            if fd != 1:
                os.dup2(fd, 1)
            if fd != 2:
                os.dup2(fd, 2)
            if fd not in (0, 1, 2):
                os.close(fd)

            # Make us a daemon. Yes, that's all it takes.
            pid = os.fork()
            if pid > 0:
                os._exit(0)
            os.setsid()
            os.chdir("/")
            pid = os.fork()
            if pid > 0:
                os._exit(0)

            # Start the command
            if isinstance(cmd, basestring):
                cmd = shlex.split(cmd)
            p = subprocess.Popen(cmd, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=lambda: os.close(pipe[1]))
            stdout = ""
            stderr = ""
            fds = [p.stdout, p.stderr]
            # Wait for all output, or until the main process is dead and its output is done.
            while fds:
                rfd, wfd, efd = select.select(fds, [], fds, 1)
                if not (rfd + wfd + efd) and p.poll() is not None:
                    break
                if p.stdout in rfd:
                    dat = os.read(p.stdout.fileno(), 4096)
                    if not dat:
                        fds.remove(p.stdout)
                    stdout += dat
                if p.stderr in rfd:
                    dat = os.read(p.stderr.fileno(), 4096)
                    if not dat:
                        fds.remove(p.stderr)
                    stderr += dat
            p.wait()
            # Return a JSON blob to parent
            os.write(pipe[1], json.dumps([p.returncode, stdout, stderr]))
            os.close(pipe[1])
            os._exit(0)
        elif pid == -1:
            self.module.fail_json(msg="unable to fork")
        else:
            os.close(pipe[1])
            os.waitpid(pid, 0)
            # Wait for data from daemon process and process it.
            data = ""
            while True:
                rfd, wfd, efd = select.select([pipe[0]], [], [pipe[0]])
                if pipe[0] in rfd:
                    dat = os.read(pipe[0], 4096)
                    if not dat:
                        break
                    data += dat
            return json.loads(data)

    def get_state(self):
        job = self.execute_command('launchctl list %s' % self.name)
        self.current_state = False if job[0] == 113 else True
        return self.current_state

    def change_state(self):
        if self.state == 'running':
            self.load()
        elif self.state == 'stopped':
            self.unload()
        else:
            self.restart()
        pass

    def need_change(self):
        if (self.current_state and self.state == 'started') or (not self.current_state and self.state == 'stopped'):
            return False
        else:
            return True

    def load(self):
        if not self.current_state:
            self.execute_command('launchctl load -w %s' % os.path.join(self.path, '%s.plist' % self.name if not self.plist_name else self.plist_name))
            self.changed = True

    def unload(self):
        if self.current_state:
            self.execute_command('launchctl unload -w %s' % os.path.join(self.path, '%s.plist' % self.name if not self.plist_name else self.plist_name))
            self.changed =True
            self.current_state = False

    def restart(self):
        if self.current_state:
            self.unload()
        self.load()

def main():
    module = AnsibleModule(
            argument_spec = dict(
                    name=dict(required=True),
                    plist_name=dict(),
                    path=dict(required=True),
                    state=dict(choices=['stopped', 'started', 'restarted'], required=True)
                )
        )

    if module.params['state'] is None and module.params['state'] is None:
        module.fail_json(msg="Neither 'state' nor 'enabled' set")

    service = ServiceOSX(module)

    result = {}
    result['name'] = service.name

    if module.params['state'] is None:
        result['changed'] = service.changed
        module.exit_json(**result)

    if service.need_change():
        service.change_state()

    result['changed'] = service.changed
    module.exit_json(**result)

from ansible.module_utils.basic import *
main()
