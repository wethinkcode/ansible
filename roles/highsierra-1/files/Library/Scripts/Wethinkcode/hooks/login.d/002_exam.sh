#!/bin/zsh

# test if user is exam, else another script will handle it
if [ "$user" = "exam" ]
then
    # simple precautions
    $syslog -s -l 5 $chmod 000 /nfs/
    $chmod 000 /nfs/

    # delete cheating possible folder
    $syslog -s -l 5 $rm -rf /tmp/*
    $rm -rf /tmp/*

    #start the firewall
    $syslog -s -l 5 $sh /Library/Scripts/Wethinkcode/hooks/fw_exam.sh
    $sh /Library/Scripts/Wethinkcode/hooks/fw_exam.sh

    # reset exam at login
    $syslog -s -l 5 $rm -f /tmp/script
    $rm -f /tmp/script
    $syslog -s -l 5 $rm -f /tmp/subject
    $rm -f /tmp/subject
    $syslog -s -l 5 $rm -rf /exam
    $rm -rf /exam
    $syslog -s -l 5 $mkdir /exam
    $mkdir /exam

    # background copy of exam's home
    $syslog -s -l 5 $cp -Rp /Library/Scripts/Wethinkcode/exam / '&'
    $cp -Rp /Library/Scripts/Wethinkcode/exam / &
    $syslog -s -l 5 $chmod 700 /exam
    $chmod 700 /exam

    $cp -r /exam/.launch/wrapper.app /usr/bin

    # custom dock bar for exam
    $profiles -I -F /Library/Scripts/Wethinkcode/exam/Library/Preferences/customdock.mobileconfig
else
    $syslog -s -l 5 'nothing to do...'
fi

