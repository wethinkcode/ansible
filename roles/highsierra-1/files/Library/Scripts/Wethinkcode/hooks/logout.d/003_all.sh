#!/bin/zsh

# some mandatory cleanup because os x doesn't want to do it...
$syslog -s -l 5 $kill -9 `$pgrep -u $user`
$kill -9 `$pgrep -u $user`
