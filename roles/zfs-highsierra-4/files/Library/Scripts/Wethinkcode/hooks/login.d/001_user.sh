#!/usr/bin/python
import os
import sys
import syslog
import commands
from pwd import getpwnam

config = { 'reset': True, 'goinfre': True, 'tmpfs': True }

def mkdir(path, mode):
	try:
		syslog.syslog(syslog.LOG_ALERT, 'Creating "%s" directory...' % path)
		os.mkdir(path, mode)
		syslog.syslog(syslog.LOG_ALERT, 'Chown %d:%d "%s" directory...' % (pw.pw_uid, pw.pw_gid, path))
		os.chown(path, pw.pw_uid, pw.pw_gid)
	except Exception as e:
		syslog.syslog(syslog.LOG_ALERT, 'mkdir(): raised an error: %s' % str(e))

def cmd(arg):
	rc = commands.getstatusoutput(arg)
	syslog.syslog(syslog.LOG_ALERT, '%s => %d (%s)' % (arg, rc[0], rc[1]))

if __name__ == '__main__':
	syslog.openlog('hook')
	user, group, home = os.environ.get('user'), os.environ.get('group'), os.environ.get('home')
	tmpfs = '/tmp/library.%s' % user
	letter = user[:1]

	if user != 'exam':
		os.chmod('/nfs', 0555)
		pw = getpwnam(user)
		syslog.syslog(syslog.LOG_ALERT, 'ARGS: user=%s, group=%s, home=%s tmpfs=%s' % (user, group, home, tmpfs))
		os.setgid(pw.pw_gid)
		os.setuid(pw.pw_uid)


		if (home.find('zfs-student-6') > -1):
#			os.system('mount -t nfs zfs-student-6.wethinkcode.co.za:/tank/users/%s/%s %s' % (letter, user, home))
			os.system('mount -t nfs 10.204.0.202:/tank/users/%s/%s %s' % (letter, user, home))
			syslog.syslog(syslog.LOG_ALERT, 'Mounting home from zfs-student-6... "%s"' % home)
		elif (home.find('zfs-student-7') > -1):
#			os.system('mount -t nfs zfs-student-7.wethinkcode.co.za:/tank/users/%s/%s %s' % (letter, user, home))
			os.system('mount -t nfs 10.204.0.206:/tank/users/%s/%s %s' % (letter, user, home))
			syslog.syslog(syslog.LOG_ALERT, 'Mounting home from zfs-student-7... "%s"' % home)
		else:
			syslog.syslog(syslog.LOG_ALERT, 'Invalid home directory... "%s"' % home)

		os.system('ln -s /sgoinfre/sgoinfre %s/Desktop/' % home)
		syslog.syslog(syslog.LOG_ALERT, 'Creating symlink on Desktop to /tank/sgoinfre')
		
		if os.path.exists(home):
			os.chdir(home)
			if os.path.isfile('.elcapitan'):
				syslog.syslog(syslog.LOG_ALERT, 'Resetting "%s" Library directory...' % home)
				os.system('rm -rf %s/Library' % home)
				os.system('rm %s/.elcapitan' % home)
				mkdir("%s/Library" % home, 0755)
				mkdir("%s/Library/Caches" % home, 0700)
				mkdir("%s/Library/Application Support" % home, 0755)
				mkdir("%s/Library/Calendars" % home, 0700)
				mkdir("%s/Library/Saved Application State" % home, 0700)

			if config.get('reset'):
				if os.path.isfile('.reset_library'):
					syslog.syslog(syslog.LOG_ALERT, 'Resetting "%s" Library directory...' % home)
					os.system('rm .reset_library')
					os.system('rm -rf %s/Library' % home)
				if os.path.isfile('.reset'):
					syslog.syslog(syslog.LOG_ALERT, 'Resetting "%s" home directory...' % home)
					os.system('rm -rf %s/.*' % home)
					os.system('rm -rf %s/*' % home)

			if config.get('goinfre'):
				if not os.path.exists('/goinfre/%s' % user):
					mkdir('/goinfre/%s' % user, 0700)
				if not os.path.islink('%s/goinfre' % home):
					cmd('ln -s "/goinfre/%s/" "%s/goinfre"' % (user, home))

			os.system('chmod 700 %s' % home)

			shits = [
				{ 'path': 'Application Support', 'mode': 0755, 'sync': True },
				{ 'path': 'Saved Application State', 'mode': 0700, 'sync': True },
				{ 'path': 'Caches', 'mode': 0700, 'sync': False },
				{ 'path': 'Calendars', 'mode': 0700, 'sync': True },
			]

			if not config.get('tmpfs'):
				for s in shits:
					if os.path.islink('%s/Library/%s' % (home, s.get('path'))):
						syslog.syslog(syslog.LOG_ALERT, 'Removing %s symlink...' % s.get('path'))
						cmd('rm "%s/Library/%s"' % (home, s.get('path')))
						if os.path.islink('%s/Library/.%s' % (home, s.get('path'))):
							cmd('mv -f "%s/Library/.%s" "%s/Library/%s"' % (home, s.get('path'), home, s.get('path')))
						else:
							mkdir('%s/Library/%s' % (home, s.get('path')), s.get('mode'))
			else:
				if not os.path.exists(tmpfs):
					syslog.syslog(syslog.LOG_ALERT, 'Creating tmpfs...')
					mkdir(tmpfs, 0700)

				for s in shits:
					if not os.path.islink('%s/Library/%s' % (home, s.get('path'))):
						os.chmod('%s/Library/%s' % (home, s.get('path')), s.get('mode'))
						if not os.path.exists('%s/Library/%s' % (home, s.get('path'))):
							mkdir('%s/Library/%s' % (home, s.get('path')), s.get('mode'))
						if s.get('sync'):
							cmd('rsync -a --delete --size-only "%s/Library/%s" "%s/"' % (home, s.get('path'), tmpfs))
						else:
							cmd('rm -rf "%s/Library/%s"' % (home, s.get('path')))
						cmd('mv -f "%s/Library/%s" "%s/Library/.%s"' % (home, s.get('path'), home, s.get('path')))
						syslog.syslog(syslog.LOG_ALERT, 'Creating "%s" symlink...' % s.get('path'))
						cmd('ln -s "%s/%s" "%s/Library/%s"' % (tmpfs, s.get('path'), home, s.get('path')))
					else:
						if not os.path.exists('%s/%s' % (tmpfs, s.get('path'))):
							mkdir('%s/%s' % (tmpfs, s.get('path')), s.get('mode'))
						else:
							syslog.syslog(syslog.LOG_ALERT, 'Already a link for "%s"' % s.get('path'))
		else:
			syslog.syslog('Home "%s" doesnt exists' % home)
