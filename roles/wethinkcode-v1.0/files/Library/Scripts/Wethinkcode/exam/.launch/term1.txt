Pour démarrer votre examen, attendez l'heure exacte de début de l'examen, et
lancez la commande "examshell" dans un terminal. Vous allez devoir la laisser
tourner et travailler dans un autre terminal.

Vous avez 10 minutes a partir de l'heure de début de l'exam pour choisir votre
projet et avoir obtenu votre premier sujet. Après ce délai, vous ne pourrez pas
faire l'examen.

La documentation du système est dans ~/docs/README.fr.md

Une fois votre examen lancé, des consignes importantes se trouveront dans
~/docs. LISEZ-LES.

Bonne chance.

================================================================================

To start your exam, wait until the exam start time then launch the "examshell"
command in a terminal. You will have to leave it running, and work in another
terminal.

You have 10 minutes after the exam start time to select your project and obtain
your first assignment. After this delay, you will not be able to do the exam.

The system's documentation is in ~/docs/README.en.md

Once your exam is started, important guidelines will be in ~/docs. READ THEM.

Good luck.
