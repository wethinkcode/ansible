#!/usr/bin/python

from syslog import syslog

class Cleaner(object):
    disk = '/Volumes/Data'
    path = '/nfs/zfs-student*/users/*/*'
    def __init__(self):
        from glob import glob
        from os.path import getmtime
        from time import time
        from datetime import timedelta, datetime
        past_week = time() - timedelta(weeks=1).total_seconds()
        self.home_directories = []
        for home_directory in glob(self.path):
            modified_time = getmtime(home_directory)
            if modified_time < past_week:
                self.home_directories.append(home_directory)
                syslog('selecting %s: not used since %s' % (home_directory, datetime.fromtimestamp(modified_time).strftime("%Y-%m-%d %H:%M:%S")))
                print 'selecting %s: not used since %s' % (home_directory, datetime.fromtimestamp(modified_time).strftime("%Y-%m-%d %H:%M:%S"))

    def pretty_size(self, size):
        for x in ['bytes','KB','MB','GB']:
            if size < 1024.0 and size > -1024.0:
                return "%3.1f%s" % (size, x)
            size /= 1024.0
        return "%3.1f%s" % (size, 'TB')

    def cleanup(self, dry_run=True):
        from shutil import rmtree
        removed = False
        for home_directory in self.home_directories:
            if not dry_run:
                rmtree(home_directory, ignore_errors=True)
                removed = True
            syslog('removing: %s...' % (home_directory))
            print 'removing: %s...' % (home_directory)
        return removed

    def disk_usage(self):
        from os import statvfs
        st = statvfs(self.disk)
        free = st.f_bavail * st.f_frsize
        used = (st.f_blocks - st.f_bfree) * st.f_frsize
        total = st.f_blocks * st.f_frsize
        syslog('%s usage: free: %s used: %s total: %s' % (self.disk,
                                                          self.pretty_size(free),
                                                          self.pretty_size(used),
                                                          self.pretty_size(total)))
        print '%s usage: free: %s used: %s total: %s' % (self.disk,
                                                         self.pretty_size(free),
                                                         self.pretty_size(used),
                                                         self.pretty_size(total))

if __name__ == '__main__':
    c = Cleaner()
    c.disk_usage()
    if c.cleanup(dry_run=False):
        c.disk_usage()
