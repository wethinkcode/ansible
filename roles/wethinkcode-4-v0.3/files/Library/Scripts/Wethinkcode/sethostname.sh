#!/bin/sh

ifconfig="/sbin/ifconfig"
perl="/usr/bin/perl"
grep="/usr/bin/grep"
nslookup="/usr/bin/nslookup"
rm="/bin/rm"
scutil="/usr/sbin/scutil"
file="/tmp/en0.ip"
interface="en0"

while true
do
    sleep 1;
    ip=`$ifconfig $interface | $grep inet | $perl -wlne 'print $1 if /inet ((:?\d{1,3}\.){3}\d{1,3})/'`
    echo "$0 trying to find out current ip: '$ip'."
    echo $ip > $file
    arpa=`$nslookup < $file 2> /dev/null | $grep 'in-addr.arpa'`
    echo "$0 trying to reverse current ip ('$ip'): '$arpa'."
    if [ "$?" != "0" ]
    then
        continue
    else
        name=`echo $arpa | $perl -wlne 'print $1 if /in-addr.arpa\s+name\s+=\s+(.+)\.wethinkcode\.co\.za/'`
        echo "$0 trying to get the reverse of current ip ('$ip'): '$name'."
        if [ "$name" != "" ]
        then
            $rm -f $file
            break
        fi
    fi
done

echo $name

echo "$0 found $name while reversing $ip which was found on en0."

# user friendly name
computername=`$scutil --get ComputerName`
if [ "$name" != "$computername" ]
then
    echo "Setting ComputerName. Old one: '$computername'."
    $scutil --set ComputerName $name
else
    echo "ComputerName already correctly set."
fi

# network name
hostname=`$scutil --get HostName`
if [ "$name.wethinkcode.co.za" != "$hostname" ]
then
    echo "Setting HostName. Old one: '$hostname'."
    $scutil --set HostName "$name.wethinkcode.co.za"
else
    echo "HostName already correctly set."
fi

# bonjour name
localhostname=`$scutil --get LocalHostName`
if [ "$name" != "$localhostname" ]
then
    echo "Setting LocalHostName. Old one: '$localhostname'."
    $scutil --set LocalHostName $name
else
    echo "LocalHostName already correctly set."
fi
defaults write /Library/Preferences/com.apple.loginwindow.plist LoginwindowText "$name.wethinkcode.co.za"
