#!/bin/sh

IDLE_TIME=$((`/usr/sbin/ioreg -c IOHIDSystem | /usr/bin/sed -e '/HIDIdleTime/ !{ d' -e 't' -e '}' -e 's/.* = //g' -e 'q'` / 1000000000))
CURRENT_USER=$(/usr/bin/stat -f '%u' /dev/console)

if [ $IDLE_TIME -ge 10800 -a $CURRENT_USER -ne 0 ]
then
    /sbin/reboot
fi
