#!/bin/zsh

# test if user is exam, else another script will handle it
if [ "$user" = "exam" ]
then
    # simple precautions
    $syslog -s -l 5 $chmod 755 /nfs/
    $chmod 755 /nfs/
    $syslog -s -l 5 $rm -rf /exam/rendu
    $rm -rf /exam/rendu
    $syslog -s -l 5 $rm -rf /exam/subject
    $rm -rf /exam/subject
    $syslog -s -l 5 $rm -f /tmp/subject
    $rm -f /tmp/subject
    $syslog -s -l 5 $rm -f /tmp/script
    $rm -f /tmp/script

    # remove all profiles (dock)
    $syslog -s -l 5 $profiles -D
    echo 'y' | $profiles -D

else
    $syslog -s -l 5 'Nothing to do...'
fi
