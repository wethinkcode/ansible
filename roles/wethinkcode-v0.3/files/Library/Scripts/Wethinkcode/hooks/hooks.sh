#!/bin/zsh

export syslog='/usr/bin/syslog'
export chmod='/bin/chmod'
export pfctl='/sbin/pfctl'
export chmod='/bin/chmod'
export rm='/bin/rm'
export mkdir='/bin/mkdir'
export cp='/bin/cp'
export id='/usr/bin/id'
export sudo='/usr/bin/sudo'
export kill='/bin/kill'
export osascript='/usr/bin/osascript'
export sleep='/bin/sleep'
export chown='/usr/sbin/chown'
export mkdir='/bin/mkdir'
export zsh='/bin/zsh'
export sh='/bin/sh'
export umount='/sbin/umount'
export rsync='/usr/bin/rsync'
export pgrep='/usr/bin/pgrep'
export df='/bin/df'
export who='/usr/bin/who'
export grep='/usr/bin/grep'
export diskutil='/usr/sbin/diskutil'
export mount='/sbin/mount'
export pkill='/usr/bin/pkill'
export profiles='/usr/bin/profiles'

export user=$1
export group=`$id -gnr $user`
export home=~$user
export home_mount=$home.mount

$syslog -s -l 5 $0 $user $group $home
