#!/usr/bin/python
import os
import sys
import syslog
import commands
from pwd import getpwnam

config = { 'reset': True, 'goinfre': True, 'tmpfs': True }

def cmd(arg):
	rc = commands.getstatusoutput(arg)
	syslog.syslog(syslog.LOG_ALERT, '%s => %d (%s)' % (arg, rc[0], rc[1]))

if __name__ == '__main__':
	syslog.openlog('hook')
	user, group, home = os.environ.get('user'), os.environ.get('group'), os.environ.get('home')
	tmpfs = '/tmp/library.%s' % user

