#!/bin/zsh

hooks_dir='/Library/Scripts/Wethinkcode/hooks/'

. $hooks_dir/hooks.sh "$@"

$syslog -s -l 5 $pfctl -d
$pfctl -d

for file in $hooks_dir/login.d/*
do
    $syslog -s -l 5 executing $file
    $file
done
