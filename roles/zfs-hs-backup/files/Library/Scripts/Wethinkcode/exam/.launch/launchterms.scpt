tell application "iTerm 2"

	activate

	set the bounds of the first window to {100, 100, 728, 1355}

	tell last terminal

		set number of columns to 80
		set number of rows to 70

		tell the last session
			set name to "Exam"
			write text "clear; cat /exam/.launch/term1.txt"
		end tell

	end tell

	set the bounds of the first window to {100, 100, 728, 1355}

end tell

